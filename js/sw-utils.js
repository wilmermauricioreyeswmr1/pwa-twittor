const actualizarCacheDinamico = (req, res, cacheDinamico) => {

    if (!res.ok) return res;

    const respuesta = caches.open(cacheDinamico).then(cache => {

        cache.put(req, res.clone());

        return res;
    });
}